
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Mahasiswa {
    private String namas;
    private String nims;
    private String alamats;

    public String getNamas() {
        return namas;
    }

    public String setNamas(String inputNamas) {
        return this.namas = inputNamas;
    }

    String getNims() {
        return nims;
    }

    String setNims(String inputNims) {
        return this.nims = inputNims;
    }

    public String getAlamats() {
        return alamats;
    }

    String setAlamats(String inputAlamats) {
        return this.alamats = inputAlamats;
    }
}

public class MainClass {

    public static void main(String[] a) {
        ArrayList biodata = new ArrayList();

        Scanner scanner = new Scanner(System.in);
        boolean next = true;
        while (next) {
            System.out.print("masukkan nim : ");
            String nims = scanner.nextLine();

            System.out.print("masukkan nama : ");
            String namas = scanner.nextLine();

            System.out.print("masukkan alamat: ");
            String alamats = scanner.nextLine();

            System.out.print("tambah lagi? (y/t) ");
            String tambah = scanner.nextLine();

            if (tambah.equalsIgnoreCase("t")) {
                next = false;
            }
            
            Mahasiswa m = new Mahasiswa();
            m.setAlamats(alamats);
            m.setNamas(namas);
            m.setNims(nims);
            biodata.add(m);
        }
        System.out.println("| Nama         | NIM                | Alamat                |");
        System.out.println("|--------------|--------------------|-----------------------|");
        for (int i = 0; i < biodata.size(); i++) {
            Mahasiswa m = (Mahasiswa) biodata.get(i);
            System.out.printf("| %-13s| %-16s| %-22s|\n", m.getNamas(), m.getNims(), m.getAlamats());
        }
        System.out.println("|--------------|--------------------|-----------------------|");
    }
}